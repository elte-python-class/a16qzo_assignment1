## Assignment 1 - Gergely Tarjan, 2020-10-31

class Protein:

    def __init__(self, protein):

        
        self.protein = protein
        
        
        # Data from header
        
        self.splitlines = self.protein.split("\n")
        self.header = self.splitlines[0]
        
        self.os = self.header.split("OS=")[1].split(" OX")[0]
        self.ox = int(self.header.split("OX=")[1].split(" GN")[0])
        self.gn = self.header.split("GN=")[1].split(" PE")[0]
        self.pe = int(self.header.split("PE=")[1].split(" SV")[0])
        self.sv = int(self.header.split("SV=")[1])
         

        # Sequence length

        self.sequence = ""

        for i,j in enumerate(self.splitlines):
            if i > 0:
                self.sequence += j

        self.size = len(self.sequence)
        

        # Name and ID
        
        self.name = (self.header.split(" OS")[0].split("|"))[2]
        self.id = (self.header.split(" OS")[0].split("|"))[1]
          
        
        # Counting amino acids with a dictionary
        
        self.aminoacidcounts = {
            
            "A" : self.sequence.count("A"),
            "C" : self.sequence.count("C"),
            "D" : self.sequence.count("D"),
            "E" : self.sequence.count("E"),
            "F" : self.sequence.count("F"),
            "G" : self.sequence.count("G"),
            "H" : self.sequence.count("H"),
            "I" : self.sequence.count("I"),
            "K" : self.sequence.count("K"),
            "L" : self.sequence.count("L"),
            "M" : self.sequence.count("M"),
            "N" : self.sequence.count("N"),
            "P" : self.sequence.count("P"),
            "Q" : self.sequence.count("Q"),
            "R" : self.sequence.count("R"),
            "S" : self.sequence.count("S"),
            "T" : self.sequence.count("T"),
            "V" : self.sequence.count("V"),
            "W" : self.sequence.count("W"),
            "Y" : self.sequence.count("Y"),
            
             }
    
    
    # String representation

    def __repr__(self):
        
        self.name = (self.header.split(" OS")[0].split("|"))[2]
        self.id = (self.header.split(" OS")[0].split("|"))[1]
        
        return str(self.name + " id: " + self.id)
      
      
    # Equality, inequality    

    def __eq__(self, other):
        return ((self.size) == (other.size))

    def __lt__(self, other):
        return ((self.size) < (other.size))

    def __gt__(self, other):
        return ((self.size) > (other.size))

    def __ge__(self, other):
        return ((self.size) >= (other.size))

    def __le__(self, other):
        return ((self.size) <= (other.size))

    def __ne__(self, other):
        return ((self.size) != (other.size))
    

# Loading .fasta file

def load_fasta(fastaFile):
        
        wholeText = open(fastaFile, "r")
        wholeRead = wholeText.read()
        wholeText.close()
        
        fastaSplit = wholeRead.split("\n>")
        fastaSplit[0] = fastaSplit[0].replace(">","")
        
        proteins = []
        for i in fastaSplit:
               proteins.append(Protein(i))
        
        return(proteins)
    

# Sorting proteins by existence
    
def sort_proteins_pe(proteins):

    proteins = sorted(proteins, key = lambda prot: prot.pe, reverse = True)

    return proteins


# Sorting proteins by amino acid content

def sort_proteins_aa(proteins, aa):

    proteins = sorted(proteins, key = lambda prot: prot.sequence.count(aa), reverse = True)

    return proteins


# Finding a given motif in proteins

def find_protein_with_motif(proteins, mot):
    
    motifList = []
    for i in proteins:
        if mot in i.sequence:
            motifList.append(i)

    return motifList
